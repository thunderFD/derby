extends Panel
signal choice_pressed

var actor_colors = {
	"Derby": Color(1,0.8,0.6),
	"You": Color(1,1,1),
	"default": Color(1,1,1)
}

#var reveal

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if $Dialogue/Speech.visible_characters < 10000:
		$Dialogue/Speech.visible_characters += 2


func hide_all():
	$Dialogue.visible = false
	$Choices.visible = false


func start_dialogue(actor = "Actor", speech = "Speech"):
	self.hide_all()
	$Dialogue.visible = true
	
	$Dialogue/Actor.text = actor
	$Dialogue/Speech.text = speech
	
	if actor in actor_colors:
		$Dialogue.modulate = actor_colors[actor]
	else:
		$Dialogue.modulate = actor_colors["default"]
	
	$Dialogue/Speech.visible_characters = 0


func start_choices(choices = []):
	self.hide_all()
	$Choices.visible = true
	
	var buttons = $Choices.get_children()
	for button in buttons:
		button.visible = false
	
	for choice_id in range(0, choices.size()):
		if choice_id >= buttons.size():
			break
		buttons[choice_id].visible = choices[choice_id]["PassCondition"]
		buttons[choice_id].text = choices[choice_id]["Dialogue"]
		buttons[choice_id].set_tooltip(choices[choice_id]["ToolTip"])

func _on_button_pressed(id):
	emit_signal("choice_pressed", id)

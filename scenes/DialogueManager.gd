extends Node2D
signal dialogue_command

onready var textbox = get_node("UILayer/TextBox")
var conditional_data = {}

func set_conditional_data(key, value):
	conditional_data[key] = value

func _ready():
	textbox.visible = false
	$Dialogue.start_dialogue()

func _process(delta):
	if Input.is_action_just_pressed("ui_dialogue_next"):
		if !textbox.get_node("Choices").visible: 
			$Dialogue.next_dialogue()


func _on_Dialogue_Dialogue_Next(ref, actor, text):
	var data = actor.split(",")
	actor =  data[0]
	data.remove(0)
	
	textbox.start_dialogue(actor, text)
	
	for command in data:
		command = command.split("=")
		emit_signal("dialogue_command", command)
			


func _on_Dialogue_Choice_Next(ref, choices):
	textbox.start_choices(choices)


func _on_Dialogue_Dialogue_Ended():
	textbox.hide_all()
	textbox.visible = false
	
	$Dialogue.start_dialogue()

func _on_Dialogue_Dialogue_Started():
	textbox.visible = true


func _on_Dialogue_Conditonal_Data_Needed():
	$Dialogue.send_conditonal_data(conditional_data)


func _on_TextBox_choice_pressed(id):
	$Dialogue.next_dialogue(id)

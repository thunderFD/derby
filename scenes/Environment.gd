extends Spatial

var face_blends = {
	"face_neutral": 	Vector2(0,0),
	"face_happy": 		Vector2(0.9,0.9),
	"face_sad": 		Vector2(-0.6,-0.9),
	"face_devastated": 	Vector2(-1,0),
	"face_bored": 		Vector2(0,1),
	"face_shocked": 	Vector2(0,-1),
	"face_funny": 		Vector2(0.9,-0.9),
	"face_laugh": 		Vector2(1,0),
	"face_angry": 		Vector2(-0.9,0.9),
	"face_scared": 		Vector2(-0.9,-0.6),
}

var face_blend_target = Vector2(0,0)
var face_lerp = 5

var eye_blends = {
	"eyes_camera":		Vector2(0,0),
	"eyes_left":		Vector2(-1,0),
	"eyes_right":		Vector2(1,0),
	"eyes_down":		Vector2(0,-1),
}

var light_blend_target = 1.0
var light_lerp = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	lerp_face_blend(delta)
	lerp_light_blend(delta)


func _on_DialogueManager_dialogue_command(command):
	print("command is: ", command)
	if len(command) == 1:
		if command[0] in face_blends:
			face_blend_target = face_blends[command[0]]		#set_face_blend(face_blends[command[0]])
		
		elif command[0] in eye_blends:
			set_eye_blend(eye_blends[command[0]])
		elif command[0] == "cover_on":
			$Camera/BlackCover.visible = true
		elif command[0] == "cover_off":
			$Camera/BlackCover.visible = false
			
	elif len(command) == 2:
		if command[0] == "light":
			light_blend_target = float(command[1])		#set_light_blend(float(command[1]))


func set_light_blend(blendpos = 0.0):
	$AnimationTree.set("parameters/lightblend/blend_position", blendpos)

func lerp_light_blend(delta):
	var prev = $AnimationTree.get("parameters/lightblend/blend_position")
	#set_light_blend(prev.linear_interpolate(light_blend_target, light_lerp * delta))
	
	set_light_blend(prev * (1 - light_lerp * delta) + light_blend_target * (light_lerp * delta))

func set_face_blend(blendpos = Vector2()):
	$Derby/AnimationTree.set("parameters/blendstate/faceblend/blend_position", blendpos)

func lerp_face_blend(delta):
	var prev = $Derby/AnimationTree.get("parameters/blendstate/faceblend/blend_position")
	set_face_blend(prev.linear_interpolate(face_blend_target, face_lerp * delta))

func set_eye_blend(blendpos = Vector2()):
	$Derby/AnimationTree.set("parameters/blendstate/eyeblend/blend_position", blendpos)